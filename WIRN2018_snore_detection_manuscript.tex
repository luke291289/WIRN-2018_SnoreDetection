%%%%%%%%%%%%%%%%%%%% author.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sample root file for your "contribution" to a contributed volume
%
% Use this file as a template for your own input.
%
%%%%%%%%%%%%%%%% Springer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% RECOMMENDED %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\documentclass[graybox]{styles/svmult}
\documentclass[runningheads,a4paper]{llncs}
% choose options for [] as required from the list
% in the Reference Guide

\usepackage{mathptmx}       % selects Times Roman as basic font
\usepackage{helvet}         % selects Helvetica as sans-serif font
\usepackage{courier}        % selects Courier as typewriter font
\usepackage{type1cm}        % activate if the above 3 fonts are
                            % not available on your system
%
\usepackage{makeidx}         % allows index generation
\usepackage{graphicx}        % standard LaTeX graphics tool
                             % when including figure files
\usepackage{multicol}        % used for the two-column index
\usepackage[bottom]{footmisc}% places footnotes at page bottom

% see the list of further useful packages
% in the Reference Guide

\makeindex             % used for the subject index
                       % please use the style svind.ist with
                       % your makeindex program

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%my package
\usepackage{amsmath}
\usepackage{subcaption}
\captionsetup{compatibility=false}
%\captionsetup[subfigure]{margin=10pt}
\usepackage{color}
%\usepackage{makecell,interfaces-makecell}
\usepackage{tablefootnote}
\usepackage{siunitx}
\usepackage{array}
\usepackage{booktabs}
\usepackage{multirow}

%my command
\newcommand{\secref}[1]{Section~\ref{#1}}
\newcommand{\figref}[1]{Fig.~\ref{#1}}
\newcommand{\tableref}[1]{\tablename~\ref{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\begin{document}
	
	\title{Convolutional Recurrent Neural Networks and Acoustic Data Augmentation for Snore Detection}
	% Use \titlerunning{Short Title} for an abbreviated version of
	% your contribution title if the original one is too long
	\author{Fabio Vesperini, Luca Romeo, Emanuele Principi, Stefano Squartini}
	% Use \authorrunning{Short Title} for an abbreviated version of
	% your contribution title if the original one is too long
	\institute{Department of Information Engineering\\Universit\`{a} Politecnica delle Marche Via Brecce Bianche, 60131, Ancona, Italy\\
	\email{f.vesperini@pm.univpm.it}}
	%
	% Use the package "url.sty" to avoid
	% problems with special characters
	% used in your e-mail or web address
	%

	\authorrunning{F. Vesperini, E. Principi, L. Romeo, S. Squartini}
	\titlerunning{CRNN and Acoustic Data Augmentation for Snore Detection}
	\maketitle

%	\abstract*{Each chapter should be preceded by an abstract (10--15 lines long) that summarizes the content. The abstract will appear \textit{online} at \url{www.SpringerLink.com} and be available with unrestricted access. This allows unregistered users to read the abstract as a teaser for the complete chapter. As a general rule the abstracts will not appear in the printed version of your book unless it is the style of your particular book or that of the series to which your book belongs.
%	Please use the 'starred' version of the new Springer \texttt{abstract} command for typesetting the text of the online abstracts (cf. source file of this chapter template \texttt{abstract}) and include them with the source files of your manuscript. Use the plain \texttt{abstract} command if the abstract is also to appear in the printed version of the book.}

	\abstract{In this paper, we propose an algorithm for snoring sounds detection based on Convolutional Recurrent Neural Networks (CRNN). The log mel energy spectrum of the audio signal are extracted from overnight recordings and are used as input to the CRNN with the aim to detect the precise onset and offset time of the sound events. Due to the high imbalance that affects the dataset between the snore events samples and the background noise, different data augmentation techniques have been evaluated. The performance of the algorithm has been assessed on the A3Snore corpus, a dataset which consists of more than 7 hours of recordings of 2 snorers and consistent environmental noise. The results are expressed in terms of Average Precision (AP) and show that the combination of CRNN and data augmentation in the raw-data domain is effective and obtain an AP up to 94.92\%.}
	
	
	
	
	
	
\section{Introduction}
\label{sec:1}

Almost a third of the time in people's life has been spent in sleep, thus the sleep quality is very important to people's health. Most of us  have experienced trouble sleeping just sometimes, while for some people sleep problems occur regularly. In those cases a sleep disorder is diagnosed. Among these, one of the most common sleep disorder \cite{sleep-disorders} is the chronic snoring.
Snoring is a noise produced when vibration occurs at several levels of the upper airway. It may be associated with various degrees of upper airway resistance. It is a highly prevalent disorder among the community: it has been confirmed that approximately 30\% of the overall population suffers from chronic snoring almost every night \cite{young1997nasal}.

Sound snoring can have a negative impact on normal physical, mental, social and emotional functioning of the person suffering from it and his bed partner \cite{blumen2012snoring}.  It is also a typical symptom for Obstructive Sleep Apnea (OSA), a chronic sleep disease sharing a frequent occurrence \cite{strollo1996obstructive}. 
OSA is characterised by posterior pharyngeal occlusion for at least ten seconds with apnea/hypopnea and attendant arterial oxyhemoglobin desaturation. If left untreated, this life-threatening sleep disorder may induce high blood pressure, coronary heart disease, pulmonary heart failure and even nocturnal death \cite{banno2007sleep}. In addition, OSA is indicated between the main causes of significant morbidity among children \cite{lumeng2008epidemiology}.

\subsection{Related Works}
\label{ssec:related-works}
Recent studies have found that acoustic signal carries important information about the snore source and obstruction site in the upper airway of OSA patients \cite{pevernagie2010acoustics}. This significant discovery has motivated several researchers to develop acoustic-based approaches that could provide inexpensive, convenient and non-invasive monitoring and screening apparatuses to be combined with traditional diagnostic tools.

The study described in \cite{cavusoglu2007efficient} consist of the identification and segmentation process by using energy and zero crossing rate (ZCR), which were used to determine the boundaries of sound segments. Episodes have been efficiently represented into two-dimensional spectral features by using principal component analysis, and classified as snores or non-snores with Robust Linear Regression (RLR). The system was tested by using the manual annotations of an Ear-Nose-Troth (ENT) specialist as a reference. The accuracy for simple snorers was found to be 97.3\% when the system was trained using only simple snorers' data. It drops to 90.2\% when the training data contain both simple snorers’ and OSA patients' data. In the case of snore episode detection with OSA patients the accuracy is 86.8\%.

In \cite{qian2015automatic} an automatic detection, segmentation and classification of snore related signals (SRS) from overnight audio recordings by combining acoustic signal processing with machine learning techniques.
The specialists have found from OSA patients four classes of SRS, connected to the sound origin mechanism. It is typically defined as the Velum-Oropharyngeal-Tongue-Epiglottis (VOTE) scheme and identifies the tissues in upper airway that are involved in the noise generation.
For classification a k-nearest neighbor (k-NN)  model si adopted for its good performance on pattern recognition. 

In the occasion of the Interspeech 2017 ComParE challenge \cite{ComParE2017} and subsequent investigations, different approaches based on Deen Neural Networks (DNN) have been presented \cite{amiriparian2017snore}, \cite{freitag2017end}, \cite{vesperini2018snore} with the aim to classify isolated snore sound events among the four classes based of the VOTE scheme.
The outline of the paper is the following: \secref{sec:proposed-approach} describes the acoustic features extraction procedure and the employed DNN architecture. In \secref{sec:experiments} are reported the dataset details, the evaluated data augmentation techniques and the experimental set-up. \secref{sec:results} reports the results of the best performing models and \secref{sec:conclusion} concludes this contribution and presents future development ideas.

\section{Proposed Approach}
\label{sec:proposed-approach}

The aim of this work is to detect snoring episodes from overnight recordings acquired in real life conditions. The method is a two step process: the acoustic spectral features extraction and the Convolutional Neural Network (CRNN) combined with Gated Recurrent Units (GRU) processing. The algorithm is evaluated on the Average Precision (AP) score.

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{img/snore_detection_3.pdf}
	\caption{The proposed approach scheme.}\label{fig:overall}
\end{figure}

\subsection{Features Extraction}
\label{ssec:feat}
The feature extraction stage operates on stereo audio signals sampled at 44.1 kHz. 
Following the results obtained in recent works related to sound event detection \cite{DCASE2017Workshop}, we use the log mel energy coefficients (Logmel) as an efficient representation of the audio signal. The stereo signal is firstly down-mixed to mono by computing the mean of the two channels. 
The resulting audio signal is split into frames with the equal to 30\,ms and a frame step of 10\,ms, then the Logmel coefficients are obtained by filtering the power spectrogram of the frame by means of a mel filter-bank, then applying a logarithmic  transformation to each sub-band energy in order to match the human perception of loudness. We used a filter bank with 40 mel scaled channels, obtaining 40 coefficients/frame. 

\subsubsection{Convolutional Recurrent Neural Network}
\label{ssec:CNN}

CRNNs used in this work are composed of four types of layers: convolutional layers, pooling layers, recurrent layers and detection layer. 

The mathematical operation of convolution has been introduced in artificial neural network layers since 1998 \cite{lecun1998gradient} for image processing. 
Recently CNNs have been often used also in audio tasks, where they exploit one input dimension to keep track of the temporal evolution of a signal \cite{vesperini2018localizing}. In this particular case results that CNNs perform the best with filter size small with respect to the input matrix, and this means the temporal context observed in these layers is typically less than two hundred milliseconds.
Each convolutional layer is followed by batch normalization per feature map \cite{ioffe2015batch}, a leaky rectified linear unit activation function (LeakyReLU) and a dropout layer \cite{srivastava2014dropout} with rate equal to $0.3$.
A feature domain max-pooling layer is then applied to the resulting feature-map, in order to enhance the relevant information from frequency bands without lose the temporal resolution of the Logmels, as proposed in \cite{cakirconvolutional}.
The extracted features over the CNN feature maps are stacked along the frequency axis.
Max-Pooling operation combined with shared weight in convolutional layers provide robustness to frequency shifts in the input features and this is crucial to overcome the problem of intra-class acoustic variability for snore events.

In the recurrent block, the stacked features resulting from the last pooling layer are fed to layers composed of GRU units \cite{chung2014empirical}, where tanh and hard sigmoid activation functions are used for update and reset gates, respectively.
GRU layers control the information flow through a gated unit structure, which depends on the layer input, the activation at the previous frame and the reset gate.
%For frame t, the total activation of GRU layer is a linear
%interpolation of previous activation h t−1 and the candidate activation ĥ t as
%%$h t = u t · h t−1 + (1 − u t ) · ĥ tù$ where u t denotes the update gate. 
%Candidate activation ĥ t is a function of h t−1 , the GRU layer’s input x t and the reset gate r t . 
% gRU activation is mainly controlled by reset gate when the GRU layer’s input x t is significantly different than in previous frames. When
%reset gate is closed (r t = 0), the candidate activation does not include any contribution from h t−1 . 
Fast response to the changes in the input and the previous activation information is fundamental for high performance in the proposed algorithm, where the task is to detect a small chunk of consecutive time frames where the target event is present. In addition, previous work \cite{valenti2017neural} demonstrates improvements provided by recurrent architectures in the sound event detection in real-life audio.

The detection layer is a feed-forward layer of composed of a single neuron with sigmoid activation function, corresponding to the probability the event onset. The layer is time distributed, this means that while computing the output of the classification layer, the same weight and bias values are used over the recurrent layer outputs for each frame.

In a comparative aim, we implemented also a CNN architecture very similar to the CRNN, the only difference being that the recurrent layers of the CRNN are
replaced with time distributed feed-forward layers with ReLU activations. In following section we will refer it as CNN.
 
%which is an extension of the Adagrad \cite{duchi2011adaptive} algorithm. It was chosen because it is well-suited for dealing with sparse data and its robustness to different choices of model hyperparameters. Furthermore no manual tuning of learning rate is required.
The neural networks training was accomplished by the AdaDelta stochastic gradient-based optimisation algorithm \cite{zeiler2012adadelta} for a maximum of 500 epochs on the binary cross entropy loss function. The optimizer parameters were set as follows: learning rate $lr=1.0$, $\rho=0.95$, $\epsilon=10^{-6}$. An early stopping strategy monitoring the validation AP score was employed in order to reduce the computational burden and avoid overfitting. % Thus if the validation AP does not increase for 20 consecutive epochs, the training is stopped and the last saved model is selected as the final model. 

\section{Experiments}
\label{sec:experiments}
In this section are described the composition of the dataset used in this work, the data augmentation techniques, the performance metrics and the experimental set-up.

\subsection{Dataset} 
\label{ssec:dataset}
The snore detection algorithm has been evaluated on the A3-SNORE dataset. In the following, a brief description of the acquisition setup and dataset splitting is provided.   

\subsubsection{Acquisition setup:}
In order to capture the overnight audio recordings a ZOOM-H1 Handy Recorder has been used. The H1's built-in X/Y microphone provides two matched unidirectional microphones set at a 90 degree angle relative to one another. The signals are stored in WAV files with a sampling rate of $44.1\ kHz$ and bit depth equal to 16.
The input gain is automatically set by the recorder to prevent overload and distortion, while the low-cut filter was enabled in order to eliminate pops, wind noise, blowing, and other kinds of low frequency rumble.


\subsubsection{Acquisition environment:}
The acquisition environment consist of a simple bedroom, with two access points (door and window). The ZOOM recorder is positioned near the patient, at same height of the bed and in line with the subject's mouth. During the recordings, the patient is the only one that can occupy the bedroom, in order to avoid contaminations on recorded audio signals. The room dimensions are reported in \figref{fig:room}.
Background events include in addition to the silence everything that is not snore, e.g. traffic noise, breathing and speech signals, house and animal noises. We acquired some samples measurements of the event-to-background (EBR) ratios, resulting equal to 6.5 dB and 1.1 dB respectively for noise to background EBR and snore to background EBR.


\begin{figure}[t]
	\centering
	\includegraphics[width=0.75\columnwidth]{img/room.pdf}
	\caption{Plant of the recording room.} 
	\label{fig:room}
\end{figure}


\subsubsection{Dataset splitting:}
The original recordings have been manually labelled, annotating the snore events onset and offset with a resolution of 1 second. The audio sequences have been divided into chunks of 10 minutes, and only those with the highest number of snore events have been used in the experiments. 
The dataset is organized into subjects, which can be respectively used as \emph{training} or \emph{validation} sets in a two fold cross validation strategy. The number of events per class in the database is strongly unbalanced as reported in (\tableref{a3snore}). Thus, the snore detection task is challenging, due to the high number of noises on the A3-SNORE dataset. 

\begin{table}[ht]
	\centering
	\caption[A3-SNORE dataset]{\textbf{A3-SNORE} - Difference of recording times for each class, divided by snorers.}
	\begin{tabular}{cccccc}
		\hline
		\multicolumn{6}{c}{\textbf{A3-SNORE dataset}} \\
		\hline
		\# & Gender & Age & Snoring (SN) & Total Duration (Tot) & Ratio (SN/Tot) \\
		\hline
		Snorer 1 & M & 48 & 33m-27s & 3h-12m-0s & 14.5\% \\
		Snorer 2 & M & 55 & 21m-21s & 3h-50m-0s & 11.1\% \\
		\hline
		\multicolumn{3}{l}{Total} &	54m-48s	& 7h-02m-0s	& 12.8\%\\
		\hline    
	\end{tabular}	
\label{a3snore} 
\end{table}

\subsection{Data Augmentation Techniques}
\label{ssec:data-augmentation}
In order to counteract the dataset unbalancing existing in the task of snoring detection. In this application, what we are really interested in is the minority class (Snoring events) other than the majority class (Background). Thus, we need to adequately train the models in order to obtain a fairly high prediction for the minority class.
The literature suggests that it is possible to augment training data in data-space or in feature-space. 
In this work, both data augmentation approach have been evaluated, by using the \emph{Synthetic Minority Over-sampling Technique} (SMOTE) \cite{chawla2002smote} in the feature space and by generating simulated data with an increased number of snore events. In the following sections, a brief description of each method is provided.

\subsubsection{Majority Class under sampling:} it is not a properly data augmentation technique but it is a fast and easy way to balance the data. It consists in randomly selecting a subset of data from the training sets in order to balance the number of sample occurrences in the two classes.

\subsubsection{SMOTE:} is an over-sampling approach in which the minority class is over-sampled by creating new synthetic examples. The minority class is over-sampled by taking each minority class sample and introducing synthetic examples along the line segments joining any/all of the \emph{k} minority class nearest neighbors (\emph{k}-NN). Depending upon the amount of over-sampling required, neighbors from the \emph{k}-NNs are randomly chosen. In particular, synthetic samples are generated in the following way: the difference between the feature vector (sample) under consideration and its nearest neighbor is multiplied by a random number between $0$ and $1$, and add it to the feature vector under consideration. In details, for a sample $x_{i}$:
\begin{equation}
x_{j}^{\text{SMOTE}} = x_{i} + (\tilde{x}_{i,k} - x_{i}) \cdot r(j)
\end{equation}
where $r(j) \in [0,1]$.
This causes the selection of a random point along the line segment between two specific features. This approach effectively forces the decision region of the minority class to become more general. %Moreover, by applying a combination of under-sampling (on majority class) and SMOTE, the initial bias of the learner towards the negative (majority) class is reversed in the favor of the positive (minority) class.

\subsubsection{Generating simulated data:} The simulated training sets have been created starting from the folds described in \secref{ssec:dataset}.
The impulse responses between the snore source and the microphones have been recreated by using the library Pyroomacoustics \cite{Scheibler2018}.
Isolated snore sounds have been taken from the Munich-Passau Snore Sound Corpus (MPSSC) dataset \cite{ComParE2017}. It is composed of 843 snore events which have been extracted and manually screened by medical experts from Drug-Induced Sleep Endoscopy (DISE) examinations of 224 subjects.
The augmented training set has been created by convolving the isolated snore sound events of the MPSSC corpus with the synthetic impulse responses. Than, the obtained signals have been mixed with the original recordings without overlap with the already present events. The artificial added event dynamic was normalized to the maximum value observed in the original signals.

The original snore/background ratio in the aforementioned signals has been increaseto to approximately 30\%, maintaining anyway a natural unbalance which is properly of this task. The resulting total duration of snore signals is 55 minutes for Snorer 1 and 56 minutes and 5 seconds for Snorer 2.

\subsection{Performance Metrics}

The performance of the algorithms has been evaluated in term of AP score, a metric that summarizes the Precision and Recall curve. AP score is calculated as follows:
\begin{equation}
\text{AP} = \sum_n (R_n-R_{n-1})P_n,
\end{equation}
where $R_n$ and $P_n$ are respectively the Recall and Precision for threshold $n$. Precision and Recall for a generic threshold are calculated as follows:
\begin{equation}
R_n = \frac{TP_n}{TP_n+FN_n}, \quad P_n = \frac{TP_n}{TP_n+FP_n},
\end{equation}
where $TP_n$ is the number of snore frames correctly detected, $FN_n$ is the number of snore frames erroneously detected as background, and $FP$ is the number of background frames erroneously detected as snoring.

\subsection{Experimental Setup}
To asses the performance of the models, we explored different hyper-parameter configurations and for each of these we repeated the whole experiments training the models both with the original data and  with  data processed with techniques described in \secref{ssec:data-augmentation}. \tableref{CNN-params} shows the hyper-parameter configurations analyzed in our experiments. They regards kernels size, kernel number and GRU units for a total of 120 experiments. In the case of CNN the number of units and layer refers to a multi layer perceptron (MLP) architecture. The experiments were conducted in a 2-fold cross-validation strategy, thus in fold 1 we used Snorer 1 as training set and Snorer 2 as validation set and in fold 2 vice-versa. The models were selected on the averaged performance based on the AP score. 
The algorithm has been implemented in the Python language using Keras \cite{chollet2015keras} as deep learning library. All the experiments were performed on a computer equipped with a 6-core Intel i7, 32\,GB of RAM and two Nvidia Titan X graphic cards. 

\begin{table}[ht]
	\centering
	\caption{Explored network layout parameters.}
	\label{CNN-params}
	\begin{tabular}{|l|r|}
%		\hline
%		\multicolumn{2}{|c|}{Network Layout Parameters}            \\ \hline
		\hline
		Convolutional Layers Number & 3                            \\ \hline
		Kernel Number               & 4, 8, 16, 32, 64                 \\ \hline
		Kernel Size                 & $5\times5$, $3\times3$, $2\times2$ \\ \hline
		Pooling Size                & $5\times1$, $4\times1$, $2\times1$ \\ \hline
		\hline
		Recurrent Layers Number     & 2, 3                            \\ \hline
		Dense Layers Number     & 2, 3                            \\ \hline
		Number Of Units             & 4, 8, 16, 32, 64                 \\ \hline
	\end{tabular}
\end{table}

\section{Results}
\label{sec:results}
In \figref{fig:results} are reported the performance of the CRNN and CNN architectures using different data augmentation techniques. In blue are depicted results with original data. The CRNN show to be effective for snore event detection yet in these conditions, although the dataset imbalance. The best performing model is composed of 3 CNN layers with respectively [64,64,64] filters of size $3\times3$ and two GRU layers of 64 units. This configuration obtain an AP up to 82.05\%, with a difference of +7.79\% with respect to the CNN.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.75\columnwidth]{img/grafTex/results.pdf}
	\caption{Results with different data augmentation techniques for the best models of the evaluated architectures.} 
	\label{fig:results}
\end{figure}

The majority class undersampling and the SMOTE techniques obtain worst performance with respect to original recordings. For majority class undersamplig this can be motived by the necessity of DNN models of a large amount of data to be trained properly, thus a reduction of data samples cannot benefits to their detection ability. Regarding the SMOTE, the performance reduction is less dramatic (-0.13\% and -1.36\% respectively for CNNs and CRNNs) but its employment remains vain. In this case the motivation can be found on the complexity to generate new samples of audio signals in the feature space which can really improve a DNN perfomance.

The addition of isolated snore samples convolved with the simulated room impulse response has a tangible beneficial effect on the examined models. In fact, with this technique we obtain an AP improvement equal to 11.18\% and 12.87\% respectively for the CNN and the CRNN. The latter obtain an AP equal to 94.92\% with an architecture composed of 3 CNN layers with respectively [64,32,32] filters of size $5\times5$ and two GRU layers of 32 units.
This model is composed of 91.553 free-parameters and occupies approximately 1.2 MB, providing to the algorithm a good scalability in an application scenario.
%\figref{fig:roc} reports the precision-recall curves of the detectors in the two validation folds. 
%Besides the overall performance, the selected CRNN exhibits also good generalization proprieties, with an AP respectively equal to 96.42\% and 93.41\% yielding a difference of $\pm 3$\% between the performance of the two snorers.
%
%
%
%\begin{figure}[h]
%
%	\centering
%	\begin{subfigure}{.40\textwidth}
%		\centering
%		\includegraphics[width=\linewidth]{img/auc/test_fold_1/precision_recall_curve.png}
%		\caption{Fold-1}
%		
%	\end{subfigure}
%	\begin{subfigure}{.4\textwidth}
%		\centering
%		\includegraphics[width=\linewidth]{img/auc/test_fold_0/precision_recall_curve.png}
%		\caption{Fold-2}		
%	\end{subfigure}
%
%	\caption{2-class Precision-Recall curves of the best model.}	
%	\label{fig:roc}
%\end{figure}

\section{Conclusion}
\label{sec:conclusion}
In this paper, a DNN algorithm based on a CNN architecture fed with Logmel spectral features extracted from the audio signal has been proposed for snore detection. The A3Snore dataset has been acquired in real world conditions, containing overnight recordings of two male  subjects and it has been used to asses the performance of the models. Different data augmentation techniques have been evaluated in order to reduce the original dataset imbalance. The models have been selected in terms of Average Precision score with a two fold cross validation strategy. Two CNN have been compared, the CNN and the CRNN architecture. The latter resulted the more effective, obtaining an AP equal to 82.05\% with the original signals and an AP up to 94.92\% by means of the addition of external isolated snore events to the training sets. 

For future work, strategies to employ weakly labelled data can be considered. Specifically, the precise annotation of existing events from overnight recordings can be onerous. Models trained in a weakly supervised fashion can help to counteract this problem without losing the state of the art accuracy.
 

\newpage

\label{sec:5}

\bibliographystyle{splncs03}
\bibliography{IEEEabrv,references}
\end{document}
